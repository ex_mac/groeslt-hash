from distutils.core import setup, Extension

groestl_hash_module = Extension('groestl_hash',
                               sources = ['groestlmodule.c',
                                          'groestl.c',
										  'sha3/groestl.c'],
                               include_dirs=['.', './sha3'])

setup (name = 'groestl_hashs',
       version = '1.0',
       description = 'Bindings for scrypt proof of work used by Groestlcoin',
       ext_modules = [groestl_hash_module])
